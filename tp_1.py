### Trabalho Pratico nr 1 de Planeamento e Gestão de Redes
### Análise de Tragego IP
### Alcides Gomes Andrade Junior


import importlib
import sys
import math
import os

class Services :
   """
   Para obter dados sobre servicos/portas do ficheiro /etc/services
   """

   services_tcp_ = {}
   services_udp_ = {}

   @staticmethod
   def init (path_etc_services='/etc/services') :
      """
      Carregar os dados dos servicos do ficheiro /etc/services
      """
      services = open(path_etc_services, 'r')
      linhas   = services.readlines()

      for linha in linhas :
         # linha de comentario ou vazia
         if linha.startswith('#') or len(linha.strip()) == 0 :
            continue

         comps           = linha.split() # separa nos espacos em branco
         nome_servico    = comps[0]
         protocolo_porta = comps[1].split('/')
         porta           = int(protocolo_porta[0])
         protocolo       = protocolo_porta[1]

         if protocolo == 'tcp' :
            Services.services_tcp_[porta] = nome_servico
         else :
            Services.services_udp_[porta] = nome_servico

   @staticmethod
   def obter_servico_por_porta (porta, protocolo='tcp') :
      """
      Obter nome de um servico de acordo com porta indicada
      @param [int] porta
      @param [str] protocolo - em que protocolo procurar o servico
      @return [str]
      """

      servico = ''
      if protocolo == 'udp' :
         servico = Services.services_udp_.get(porta)
      else :
         servico = Services.services_tcp_.get(porta)

      return 'Desconhecido' if servico is None else servico


class Protocols :
   """
   Para obter dados sobre protocolos de Internet, do ficheiro /etc/protocols
   """

   protocols_ = {}

   @staticmethod
   def init (path_etc_protocols='/etc/protocols') :
      """
      Carregar os dados do ficheiro
      """

      protocols = open(path_etc_protocols, 'r')
      linhas    = protocols.readlines()

      for linha in linhas :
         # linha de comentario ou vazia
         if linha.startswith('#') or len(linha.strip()) == 0 :
            continue

         comps              = linha.split() # separa nos espacos em branco
         nome               = comps[0]
         numero             = int(comps[1])
         Protocols.protocols_[numero] = nome

   @staticmethod
   def obter_protcolo_por_numero (numero) :
      """
      Obter nome de protocolo de acordo com numero indicado
      @param [int] numero
      @return [str]
      """

      nome = Protocols.protocols_.get(numero)
      return 'Desconhecido' if nome is None else nome


def parse_linha (linha) :
   """
   Cada linha possui os seguintes dados, separados por espaços:
      - id da interface do emissor
      - tempo da captura em segundos
      - endereco ip do emissor
      - endereco ip do receptor
      - comprimento do pacote em bytes
      - numero do protocolo
      - porta de origem
      - porta de destino
      - flags
      - numero de sequencia
      - numero de confirmacao
      - advertised window

   @param [string] linha
   @return [tuple] tupla com as componentes lidas
   """

   # mascaras dos flags
   MASK_FLAG_URG = 32 # 0b100000
   MASK_FLAG_ACK = 16 # 0b010000
   MASK_FLAG_PSH = 8  # 0b001000
   MASK_FLAG_RST = 4  # 0b000100
   MASK_FLAG_SYN = 2  # 0b000010
   MASK_FLAG_FIN = 1  # 0b000001


   comps = linha.split(' ')
   resultado = {}
   resultado['id'] = int(comps[0])
   resultado['tempo'] = float(comps[1])
   resultado['end_origem'] = int(comps[2])
   resultado['end_destino'] = int(comps[3])
   resultado['tamanho'] = int(comps[4])

   nome_protocolo = Protocols.obter_protcolo_por_numero(int(comps[5]))
   if nome_protocolo is None :
      resultado['protocolo'] = int(comps[5])
   else :
      resultado['protocolo'] = nome_protocolo

   resultado['porta_origem']  = int(comps[6])
   resultado['porta_destino'] = int(comps[7])
   resultado['servico_dest'] = Services.obter_servico_por_porta(resultado['porta_destino'])
   resultado['servico_orig'] = Services.obter_servico_por_porta(resultado['porta_origem'])

   flags = int(comps[8], base=16) if nome_protocolo == 'tcp' else 0
   resultado['flag_urg'] = flags & MASK_FLAG_URG
   resultado['flag_ack'] = flags & MASK_FLAG_ACK
   resultado['flag_psh'] = flags & MASK_FLAG_PSH
   resultado['flag_rst'] = flags & MASK_FLAG_RST
   resultado['flag_syn'] = flags & MASK_FLAG_SYN
   resultado['flag_fin'] = flags & MASK_FLAG_FIN
   resultado['nr_seq'] = int(comps[9])
   resultado['nr_conf'] = int(comps[10])
   resultado['advertised_window'] = int(comps[11])

   return resultado


def mostrar_graficos (packs_tcp, nr_packs_tcp, packs_udp, nr_packs_udp, packs_http, fluxos_tcp, fluxos_udp) :
   import pygal

   print('A gerar gráficos...')

   # lista de tamanhos dos pacotes TCP lidos
   xs_tcp = sorted(list(packs_tcp.keys()))
   # frequencia de pacotes de cada tamanho
   ys_tcp = []
   for x in xs_tcp :
      ys_tcp.append((packs_tcp[x]/nr_packs_tcp, x, x+0.5))

   # lista de tamanhos dos pacotes UDP lidos
   xs_udp = sorted(list(packs_udp.keys()))
   # frequencia de pacotes de cada tamanho
   ys_udp = []
   for x in xs_udp :
      ys_udp.append((packs_udp[x]/nr_packs_udp, x, x+0.5))

   ys_http = []
   for p in packs_http :
      if p['porta_origem'] == 80 : # resposta http
         t = p['tempo']
         ys_http.append((p['nr_seq'], p['tempo'], p['tempo']+0.0001))

   ##  graficos de fluxos tcp por comprimento
   # comprimentos de fluxos tcp
   xs_fluxos_tcp = sorted(list(fluxos_tcp.keys()))
   # obter quantidade de fluxos para cada comprimento
   ys_fluxos_tcp = []
   for x in xs_fluxos_tcp :
      ys_fluxos_tcp.append((fluxos_tcp[x], x, x+0.5))

   # comprimentos de fluxos udo
   xs_fluxos_udp = sorted(list(fluxos_udp.keys()))
   # obter quantidade de fluxos para cada comprimento
   ys_fluxos_udp = []
   for x in xs_fluxos_udp  :
      ys_fluxos_udp.append((fluxos_udp[x], x, x+0.5))

   histograma_0 = pygal.Histogram()
   histograma_1 = pygal.Histogram()
   histograma_2 = pygal.Histogram()
   histograma_3 = pygal.Histogram()
   histograma_0.add('Número de pacotes TCP por tamanho (em bytes)', ys_tcp)
   histograma_1.add('Número de pacotes UDP por tamanho (em bytes)', ys_udp)
   histograma_2.add('Respostas HTTP em função do tempo de receção', ys_http[:50])
   histograma_3.add('Número de fluxos UDP por comprimento (do fluxo)', ys_fluxos_udp)
   histograma_3.add('Número de fluxos TCP por comprimento (do fluxo)', ys_fluxos_tcp)
   histograma_0.render_to_file('graficos_tcp.svg')
   histograma_1.render_to_file('graficos_udp.svg')
   histograma_2.render_to_file('graficos_http.svg')
   histograma_3.render_to_file('graficos_fluxos.svg')
   print('Gráficos guardados no ficheiro: graficos_tcp.svg, graficos_udp.svg, graficos_http.svg e graficos_fluxos.svg')


if __name__ == '__main__' :

   path_etc_services  = '/etc/services'
   path_etc_protocols = '/etc/protocols'
   if os.name == 'nt' :
      # windows nao possui /etc/ services nem /etc/protocols
      # por isso procurar no diretorio atual
      path_etc_services  = 'services'
      path_etc_protocols = 'protocols'
      print('A procurar ficheiros "services" e "protocols" neste diretório')

   Services.init(path_etc_services)
   Protocols.init(path_etc_protocols)

   nr_pacotes_por_protocolo = {}
   nr_bytes_por_protocolo   = {}
   nr_de_pacotes_udp_por_comprimento = {}
   nr_de_pacotes_tcp_por_comprimento = {}
   nr_de_acks = 0
   nr_fluxos_tcp = 0
   nr_fluxos_tcp_por_comprimento = {}
   nr_fluxos_udp = 0
   nr_fluxos_udp_por_comprimento = {}
   nr_outros_fluxos = 0
   nr_pacotes = 0
   nr_bytes = 0
   nr_packs_tcp = 0
   nr_packs_udp = 0

   primeiro_protocolo_mais_freq = None
   segundo_protocolo_mais_freq  = None
   terceiro_protocolo_mais_freq = None

   primeiro_protocolo_com_mais_bytes = None
   segundo_protocolo_com_mais_bytes  = None
   terceiro_protocolo_com_mais_bytes = None

   ultimo_endereco_origem  = ''
   ultimo_endereco_destino = ''
   ultima_porta_origem     = ''
   ultima_porta_destino    = ''
   ultimo_protocolo_lido   = ''
   contagem_fluxo = 1

   nr_pacotes_ftp  = 0
   nr_pacotes_smtp = 0
   nr_pacotes_http = 0
   nr_pacotes_ntp  = 0
   nr_pacotes_dns  = 0
   nr_pacotes_prot_desc = 0

   pacotes_http_apos_tempo_t = []

   logs = open('log.txt')
   conteudo = logs.readlines()
   for linha in conteudo :
      comps = parse_linha(linha)
      nr_pacotes += 1
      nr_bytes   += comps['tamanho']

      # contar o nr de pacotes por protocolo
      if nr_pacotes_por_protocolo.get(comps['protocolo']) is None :
         nr_pacotes_por_protocolo[comps['protocolo']] = 1
      else :
         nr_pacotes_por_protocolo[comps['protocolo']] += 1

      # contar o nr  de bytes por protocolo
      if nr_bytes_por_protocolo.get(comps['protocolo']) is None :
         nr_bytes_por_protocolo[comps['protocolo']] = comps['tamanho']
      else :
         nr_bytes_por_protocolo[comps['protocolo']] += comps['tamanho']

      # ver se eh um pacote ack puro
      # ack puro nao possui dados - apenas cabecalhos ip (20 bytes)
      # e tcp (+ 20 types)
      if comps['flag_ack'] != 0  and comps['tamanho'] == 40 :
         nr_de_acks += 1

      # ver os 3 protocolos mais frequentes
      nr_pacotes_prot = nr_pacotes_por_protocolo[comps['protocolo']]
      if primeiro_protocolo_mais_freq is None or \
      nr_pacotes_por_protocolo[primeiro_protocolo_mais_freq] <= nr_pacotes_prot :
         # ver trocas de posicao
         if segundo_protocolo_mais_freq == comps['protocolo'] :
            segundo_protocolo_mais_freq = primeiro_protocolo_mais_freq
         elif terceiro_protocolo_mais_freq == comps['protocolo'] :
            terceiro_protocolo_mais_freq = primeiro_protocolo_mais_freq
         primeiro_protocolo_mais_freq = comps['protocolo']
      elif segundo_protocolo_mais_freq is None or \
      nr_pacotes_por_protocolo[segundo_protocolo_mais_freq] <= nr_pacotes_prot :
         # ver trocas de posicao
         if terceiro_protocolo_mais_freq == comps['protocolo'] :
            terceiro_protocolo_mais_freq = primeiro_protocolo_mais_freq
         segundo_protocolo_mais_freq = comps['protocolo']
      elif terceiro_protocolo_mais_freq is None or \
      nr_pacotes_por_protocolo[terceiro_protocolo_mais_freq] <= nr_pacotes_prot :
         terceiro_protocolo_mais_freq = comps['protocolo']

      # ver os 3 protocolos com mais bytes de dados transmitidos
      nr_bytes_prot = nr_bytes_por_protocolo[comps['protocolo']]
      if primeiro_protocolo_com_mais_bytes is None or \
      nr_bytes_por_protocolo[primeiro_protocolo_com_mais_bytes] <=  nr_bytes_prot :
         # ver trocas de posicao
         if segundo_protocolo_com_mais_bytes == comps['protocolo'] :
            segundo_protocolo_com_mais_bytes = primeiro_protocolo_com_mais_bytes
         elif terceiro_protocolo_com_mais_bytes == comps['protocolo'] :
            primeiro_protocolo_com_mais_bytes = primeiro_protocolo_com_mais_bytes
         primeiro_protocolo_com_mais_bytes = comps['protocolo']
      elif segundo_protocolo_com_mais_bytes is None or \
      nr_bytes_por_protocolo[segundo_protocolo_com_mais_bytes] <= nr_bytes_prot :
         # ver trocas de posicao
         if terceiro_protocolo_com_mais_bytes == comps['protocolo'] :
            terceiro_protocolo_com_mais_bytes = primeiro_protocolo_com_mais_bytes
         segundo_protocolo_com_mais_bytes = comps['protocolo']
      elif terceiro_protocolo_com_mais_bytes is None or \
      nr_bytes_por_protocolo[terceiro_protocolo_com_mais_bytes] <= nr_bytes_prot :
         terceiro_protocolo_com_mais_bytes = comps['protocolo']


      # verificar a distribuicao de tamanhos de pacotes tcp e udp
      if comps['protocolo'] == 'tcp' :
         nr_packs_tcp += 1
         if nr_de_pacotes_tcp_por_comprimento.get(comps['tamanho']) is None :
            nr_de_pacotes_tcp_por_comprimento[comps['tamanho']] = 1
         else :
            nr_de_pacotes_tcp_por_comprimento[comps['tamanho']] += 1
      elif comps['protocolo'] == 'udp' :
         nr_packs_udp += 1
         if nr_de_pacotes_udp_por_comprimento.get(comps['tamanho']) is None :
            nr_de_pacotes_udp_por_comprimento[comps['tamanho']] = 1
         else :
            nr_de_pacotes_udp_por_comprimento[comps['tamanho']] += 1

      # verificar fim de um fluxo
      if contagem_fluxo >= 3 and \
      (ultimo_endereco_origem != comps['end_origem'] or \
      ultimo_endereco_destino != comps['end_destino'] or \
      ultima_porta_origem != comps['porta_origem'] or \
      ultima_porta_destino != comps['porta_destino'] or \
      ultimo_protocolo_lido != comps['protocolo']) :
         if ultimo_protocolo_lido == 'tcp' :
            nr_fluxos_tcp += 1
            # incrementar nr de fluxos tcp com o comprimento do que terminou agora
            if nr_fluxos_tcp_por_comprimento.get(contagem_fluxo) is None :
               nr_fluxos_tcp_por_comprimento[contagem_fluxo] = 1
            else :
               nr_fluxos_tcp_por_comprimento[contagem_fluxo] += 1
         elif ultimo_protocolo_lido == 'udp' :
            nr_fluxos_udp += 1
            # incrementar nr de fluxos udp com o comprimento do que terminou agora
            if nr_fluxos_udp_por_comprimento.get(contagem_fluxo) is None :
               nr_fluxos_udp_por_comprimento[contagem_fluxo] = 1
            else :
               nr_fluxos_udp_por_comprimento[contagem_fluxo] += 1
         else:
            nr_outros_fluxos += 1
         contagem_fluxo = 1 # reiniciar fluxo

      # contar fluxo atual
      elif ultimo_endereco_origem == comps['end_origem'] and \
      ultimo_endereco_destino == comps['end_destino'] and \
      ultima_porta_origem == comps['porta_origem'] and \
      ultima_porta_destino == comps['porta_destino'] and \
      ultimo_protocolo_lido == comps['protocolo'] :
         contagem_fluxo += 1

      # sequencia terminou sem fluxo
      else : contagem_fluxo = 1

      servico_dest = comps['servico_dest']
      servico_orig = comps['servico_orig']
      if servico_dest == 'http' or servico_orig == 'http' :
         nr_pacotes_http += 1
      elif servico_dest == 'ftp' or servico_orig == 'ftp' :
         nr_pacotes_ftp += 1
      elif servico_dest == 'smtp' or servico_orig == 'smtp' :
         nr_pacotes_smtp += 1
      elif servico_dest == 'ntp' or servico_orig == 'smtp':
         nr_pacotes_ntp += 1
      elif servico_dest == 'domain' or servico_orig == 'domain':
         nr_pacotes_dns += 1
      elif servico_dest == 'Desconhecido' :
         nr_pacotes_prot_desc += 1

      if (comps['porta_origem'] == 80 or comps['porta_destino'] == 80) and comps['tempo'] >= 990048367.932311:
         pacotes_http_apos_tempo_t.append(comps)

      ultimo_endereco_origem  = comps['end_origem']
      ultimo_endereco_destino = comps['end_destino']
      ultima_porta_origem     = comps['porta_origem']
      ultima_porta_destino    = comps['porta_destino']
      ultimo_protocolo_lido   = comps['protocolo']


   if nr_pacotes == 0 :
      print('Nenhum pacote encontrado')
      sys.exit()

   # mostrar resultados
   print('Número de pacotes analisados: {}'.format(nr_pacotes))
   if primeiro_protocolo_mais_freq != None :
      freq = nr_pacotes_por_protocolo[primeiro_protocolo_mais_freq]/nr_pacotes
      print('O protocolo mais frequente ({}) representa {:.2f}% dos pacotes'.format(
         primeiro_protocolo_mais_freq, freq*100))
   if segundo_protocolo_mais_freq != None :
      freq = nr_pacotes_por_protocolo[segundo_protocolo_mais_freq]/nr_pacotes
      print('O segundo protocolo mais frequente ({}) representa {:.2f}% dos pacotes'.format(
         segundo_protocolo_mais_freq, freq*100))
   if terceiro_protocolo_mais_freq != None :
      freq = nr_pacotes_por_protocolo[terceiro_protocolo_mais_freq]/nr_pacotes
      print('O terceiro protocolo mais frequente ({}) representa {:.2f}% dos pacotes'.format(
         terceiro_protocolo_mais_freq, freq*100))

   print('')

   print('Quantidade total de bytes nos pacotes: {}'.format(nr_bytes))
   if primeiro_protocolo_com_mais_bytes != None :
      freq = nr_bytes_por_protocolo[primeiro_protocolo_com_mais_bytes]/nr_bytes
      print('O protocolo com mais bytes ({}) representa {:.2f}% dos dados'.format(
         primeiro_protocolo_com_mais_bytes, freq*100))
   if segundo_protocolo_com_mais_bytes != None :
      freq = nr_bytes_por_protocolo[segundo_protocolo_com_mais_bytes]/nr_bytes
      print('O segundo protocolo com mais bytes ({}) representa {:.2f}% dos dados'.format(
         segundo_protocolo_com_mais_bytes, freq*100))
   if terceiro_protocolo_com_mais_bytes != None :
      freq = nr_bytes_por_protocolo[terceiro_protocolo_com_mais_bytes]/nr_bytes
      print('O terceiro protocolo com mais bytes ({}) representa {:.2f}% dos dados'.format(
         terceiro_protocolo_com_mais_bytes, freq*100))

   print('')

   print('Percentagens dos pacotes que representam os serviços(aplicações) smtp, http, ftp, ntp, dns e Desconhecido')
   print(' smtp         - {:.5f}%'.format((nr_pacotes_smtp/nr_pacotes)*100))
   print(' http         - {:.5f}%'.format((nr_pacotes_http/nr_pacotes)*100))
   print(' ftp          - {:.5f}%'.format((nr_pacotes_ftp/nr_pacotes)*100))
   print(' ntp          - {:.5f}%'.format((nr_pacotes_ntp/nr_pacotes)*100))
   print(' dns          - {:.5f}%'.format((nr_pacotes_dns/nr_pacotes)*100))
   print(' Desconhecido - {:.5f}%'.format((nr_pacotes_prot_desc/nr_pacotes)*100))

   print('')

   print('Percetagem dos que pacotes que são ACK\'s (puros) é {:.5f}%'.format((nr_de_acks/nr_pacotes)*100))

   print('')

   print('Quantidade total de fluxos - {}'.format(nr_fluxos_tcp + nr_fluxos_udp + nr_outros_fluxos))
   print('Quantidade de fluxos TCP - {}'.format(nr_fluxos_tcp))
   print('Quantidade de fluxos UDP - {}'.format(nr_fluxos_udp))

   if importlib.find_loader('pygal') is not None :
      mostrar_graficos(
         nr_de_pacotes_tcp_por_comprimento,
         nr_packs_tcp,
         nr_de_pacotes_udp_por_comprimento,
         nr_packs_udp,
         pacotes_http_apos_tempo_t,
         nr_fluxos_tcp_por_comprimento,
         nr_fluxos_udp_por_comprimento
      )
   else :
      print('Deve instalar a biblioteca "pygal" para poder gerar os gráficos')
